//dependencies
const jwt = require('jsonwebtoken');

// secret -Signature of the app you are working on - auth, when you declare a secret, you cannot change it. 
const secret = "CourseBookingAPI";



module.exports.createAccessToken = (user) => {
	console.log(user);

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	//sign() method for creating jwt, (what data,secret. empty)
	//token will not be tampered
	return jwt.sign(data, secret, {})
}

/*
1. You can only get access token when a user logs in the app w coorect credentials

2. As a user, you can only get your own details from your own token from logging in

3. JWT is not meant fro sensitive data

4. Passport like, use around the apps to access certain features meant for your type of User 

** every visit you get new access token
*/

//June 2
	// if tampered ung narereceive 
	// next -method that we include for

// undefined === "undefined"
//goal is to check if the token exist or not tampered
//next() has been called, for the next process
	module.exports.verify = (req, res, next) => {
		//req.headers.authorization - handles the token; contains jwt


		let token = req.headers.authorization
		
		//typeof result is a string

		if(typeof token === "undefined"){
			return res.send({auth: "Failed. No token."})
		}else{
			console.log(token);
			//cutting the jwt bearer, will start from "ey"
			// we can slice because its  a string

			token = token.slice(7, token.length)
			console.log(token)
			//decodedToken - reassign the token created
			// using jwt method

			jwt.verify(token, secret, (err, decodedToken) => {
				//err is object that has property inisde
				if(err){
					return res.send({
						auth: "Failed",
						message: err.message
					})
				} else {

					//controller user )req.user)
					console.log(decodedToken);
					req.user = decodedToken
					next();
					//next()->proceed to the next middleware or controller //process
				}
			})
		}
	};

//verifying an admin

module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next();
	}else{
		return res.send({
			auth: "Failed",
			message: "Action is Forbidden"
		})
	}
};


//express is flexible and accomadate what are needed