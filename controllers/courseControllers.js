const Course = require('../models/Course')

module.exports.addCourse = (req, res) => {
	console.log(req.body);

	let newCourse = new Course ({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		});

	
	newCourse.save().then(course => res.send(course)).catch(err => res.send(err));
};


module.exports.getAllCourses = (req, res) => {
	Course.find({}).then(result => res.send(result)).catch(err => res.send(err));
};

//act 3

module.exports.getSingleCourse = (req, res) => {
	Course.findById(req.params.id).then(result => res.send(result)).catch(err => res.send(err));
};


// when you are updating table , make sure to initialize a new variable where you can store the new properties, values for the table/collection you want to update
module.exports.updateCourse = (req, res) => {
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	//id user is the one updating use req.user.id
	Course.findByIdAndUpdate(req.params.id, updates, {new: true}).then(updatedCourse => res.send(updatedCourse)).catch(err => (err))
};

//archive course

module.exports.archiveCourse = (req, res) => {
	let updates = {
		isActive: false
	}
	Course.findByIdAndUpdate(req.params.id, updates, {new:true}).then(archivedCourse => res.send(archivedCourse)).catch(err => (err))
};

//active course

module.exports.activeCourse = (req, res) => {
	let updates = {
		isActive: true
	}
	Course.findByIdAndUpdate(req.params.id, updates, {new: true}).then(activatedCourse => res.send(activatedCourse)).catch(err => (err))
};

module.exports.getAllActiveCourse = (req, res) => {
	Course.find({isActive: true}).then(result => res.send(result)).catch(err => res.send(err));
}

//get courses via name [section]

module.exports.findCoursesByName = (req, res) => {

	console.log(req.body) //contain the name of the course you are looking for
	//check course model to find the query using criteria using regex, 
	Course.find({name: {$regex: req.body.name, $options: '$i'}}).then(result => {
		if(result.length === 0){
			return res.send('No courses found.')
		}else{
			return res.send(result)
		}
	})
}