//dependencies


//relative path, model creation
const User = require('../models/User');

//hashing of password
const bcrypt = require('bcryptjs');

//authentication
const auth = require('../auth');

//for course mode
const Course = require('../models/Course');


//register user
module.exports.registerUser = (req, res) => {
	console.log(req.body); //assurance where you will get the data


	//password hashing, use hashSync method
	/*
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>) 

		saltrounds- count on how many times your string to be hashed is randomize

		let password = req.body.password
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW

	});

	newUser.save().then(user => res.send(user)).catch(err => res.send(err));
};

//retrieve all users

module.exports.getAllUsers = (req, res) => {

	User.find({}).then(result => res.send(result)).catch(err => res.send(err));
};

//login user controller

module.exports.loginUser = (req, res) => {
		console.log(req.body);
		/*
			1. find user by email
			2. if user found check the password
			3. if user cannot be found, send message to the client
			4. if found upon checking and pass is the same, generate the access token. If not, reject by sending a message to the client
		*/
	User.findOne({email: req.body.email}).then(foundUser => {
		if(foundUser === null){
			return res.send("No user found in the database.")
		}else{

			//bcrypt method, compareSync. uses 2 value that you want to compare. Boolean result
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

			//no need comparison because of truthy values
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			}else{
				return res.send("Incorrect password please try again.")
			}
		}
	})
	.catch(err => res.send(err));
};

module.exports.getUserDetails = (req, res) => {
	//auth js
	console.log(req.user);
	
	//find the login users document from our database and send it to client by its id
	//decoded token have id: email: isAdmin:
	User.findById(req.user.id).then(result => res.send(result)).catch(err => res.send(err))
};


module.exports.checkEmail = (req, res) => {
		console.log(req.body);
	User.findOne({email: req.body.email}).then(foundEmail => {
		if(foundEmail === null){
			return res.send("Email is available.")
		}else{
			return res.send("Email is already registered.")
		}
	})
	.catch(err => res.send(err));
};

// module.exports.findUserByEmail = (req, res) => {
// 	User.findOne({email: req.body.email}).then(result => res.send(result)).catch(err => res.send(err))
// };

module.exports.findUserByEmail = (req, res) => {
	User.findOne({email: req.body.email}).then(result => {
		if(result === null){
			return res.send("Cannot find user")
		}else{
			return res.send(result)
		}
	})
		.catch(err => res.send(err));
};

//updating user details

module.exports.updateUserDetails = (req, res) => {
	console.log(req.body); // input for new values
	console.log(req.user.id); //if user is already logged in
	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	};

	//new:true reflect the updated data
	//where to get updates, id, options
	//findById so you will not go through params
	User.findByIdAndUpdate(req.user.id, updates, {new:true}).then(updatedUser => res.send(updatedUser)).catch(err => res.send(err));

};

//update admin

module.exports.updateAdmin = (req, res) => {
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //this will be the id of the user we want to update

	let updates = {
		isAdmin: true
	}

	//inside where to update
	User.findByIdAndUpdate(req.params.id, updates, {new:true}).then(updatedUser => res.send(updatedUser)).catch(err => res.send(err));
};

// [section enrollment]

	/*
		steps: - Look for user by its id
				 > push details of the course we're trying to enroll in. We'll push to a new subdocument in our user. 
			   - Look for the course by its id
			   	 > push detail of the user/ enrollee who's trying to enroll. We'll push to a new enrollees subdocument in our course.
			   - When both saving of documents are successful, we send a message to the client. 
	*/
//makes the fubction asynchronous, 
// 
module. exports.enroll = async (req, res) => {
	console.log(req.user.id) // the user's id from the decoded token after verify
	console.log(req.body.courseId) // the course from our request body
	console.log(req);

	//user is under the req module
	if(req.user.isAdmin){
		return res.send('Action Forbidden');
	}
	//once see the id user, embed the course

	//store in a var; await = boolean in the process 
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		console.log(user); //VALUES of the console

		let newEnrollment = {
			courseId: req.body. courseId
		}
		//. notation
			user.enrollments.push(newEnrollment)

			return user.save().then(user => true).catch(err => err.message)
			//res.send - check the process, 
			//create a condition if false

	})


	//intayin nya macomplete unf update sa docs
	// why use async?
	// 2 models are being control, 2 separate process
	// hindi mag antayan
	// di sabay na tatakbo

	//if embedded course is not push properly
		//false condition, descripancy
		//optional
		if(isUserUpdated !== true){
			return res.send({message: isUserUpdated})
		}

		//can have a multiple awaits

		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

			console.log(course) //

			let enrollee = {
				userId: req.user.id
			}
			course.enrollees.push(enrollee)

			return course.save().then(course => true).catch(err => err.message)
		})


		if(isCourseUpdated !== true){
			return res.send({message: isCourseUpdated})
		}

		if(isUserUpdated && isCourseUpdated){
			return res.send({message: 'User enrolled successfully'})
		}
};

//get enrollments

module.exports.getEnrollments = (req, res) => {
	//be specific of return

	User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err))
};