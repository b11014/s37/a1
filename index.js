//dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
//port
const port = 4000;
//server
const app = express();

//db
//mongodb+srv://admin:<password>@wdc028-course-booking.arbsv.mongodb.net/?retryWrites=true&w=majority

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.arbsv.mongodb.net/course-booking-182?retryWrites=true&w=majority",{

	useNewUrlParser: true,
	// allow users to fall back to the old parser if they find a bug in the new parser
	useUnifiedTopology: true
	//behavioral changes

});

//db connection notif

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));

db.once("open", () => console.log("Successfully connected to MongoDB"));

//middlewares
//.use method enables us to use express methods
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//Group routes section
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const courseRoutes = require('./routes/courseRoutes');
app.use('/courses', courseRoutes);

// port listener
app.listen(port, () => console.log(`Server is running at port ${port}.`))


