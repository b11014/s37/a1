const mongoose = require('mongoose');

/*User
firstName - string,
lastName - string,
email - string,
password - string,
mobileNo - string,
isAdmin - boolean
	default: false

enrollments: [
	{
		courseId: string,
		status: string,
		default: "Enrolled"
		dateEnrolled: date,
	}
];*/



let userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
	{
		courseId: {
			type: String,
			required: [true, "Course Id is required."]
		},
		status: {
			type: String,
			default: "Enrolled"
		},
		dateEnrolled: {
			type: Date,
			default: new Date()
		}

	}
	]
});

module.exports = mongoose.model("User", userSchema);