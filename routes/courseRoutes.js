const express = require('express');
const router = express.Router();

const courseControllers = require('../controllers/courseControllers')

//add Course
const auth = require('../auth');


// it will apply the functions verify, verifyadmin from auth.js. 
const {verify, verifyAdmin} = auth;


//middleware verify process when you are logged in
//verifyAdmin, logged in; if feature is for admin
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

router.get('/', courseControllers.getAllCourses);

//use id to locate the a single course you want to update
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);

//archive course
router.put('/archiveCourse/:id', verify, verifyAdmin, courseControllers.archiveCourse);

//activate course
router.put('/activateCourse/:id', verify, verifyAdmin, courseControllers.activeCourse);

//get All active course
router.get('/getActiveCourses', courseControllers.getAllActiveCourse);

//find courses by name
router.post('/findCoursesByName', courseControllers.findCoursesByName);











module.exports = router;
