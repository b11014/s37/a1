// dependencies
const express = require('express');

//http method
const router = express.Router();

//imported
//modules
const userControllers = require('../controllers/userControllers');

//
const auth = require('../auth');
//object destructuring from auth module

//so we can use it easily as middleware. Middleware for the routes
//when destructuring get property name, because function is their property name
const {verify, verifyAdmin} = auth;


//routes

//register user
router.post('/', userControllers.registerUser);

//get all users
router.get('/', userControllers.getAllUsers);

//login user

router.post('/login', userControllers.loginUser);

//profile view
//user details(get) auth; use verify for the token and controller connection -middleware
router.get('/getUserDetails', verify, userControllers.getUserDetails);

router.post('/checkEmail', userControllers.checkEmail);

router.post('/findUserByEmail', userControllers.findUserByEmail);

router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

router.put('/updateUserAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);


//routes for enrollment
router.post('/enroll', verify, userControllers.enroll);

//routes get enrollment
router.get('/getEnrollments', verify, userControllers.getEnrollments);









module.exports = router;

